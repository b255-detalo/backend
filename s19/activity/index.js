
/*
    
    1. Create a function called login which is able to receive 3 parameters called username,password and role.
        -add an if statement to check if the the username is an empty string or undefined or if the password is an empty string or undefined or if the role is an empty string or undefined.
            -if it is, return a message in console to inform the user that their input should not be empty.
        -add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:
                -if the user's role is admin, return the following message:
                    "Welcome back to the class portal, admin!"
                -if the user's role is teacher,return the following message:
                    "Thank you for logging in, teacher!"
                -if the user's role is a rookie,return the following message:
                    "Welcome to the class portal, student!"
                -if the user's role does not fall under any of the cases, as a default, return a message:
                    "Role out of range."
*/


// ---------------------------------------------------------------------

let username;
let password;
let role;

function login(username, password, role) {

// Asking 3 inputs from the user via 3 prompts.
 username = prompt("Enter username: ") .toLowerCase();
 password = prompt("Enter password: ") .toLowerCase();
 role = prompt("Enter role: ") .toLowerCase();

console.log("login:(" + username + "," + password + "," + role + ")" );

	if (username === '' || password === '' || role === '' ) {
		console.log("Inputs must not be empty");
	} else {

	switch(role) {

		case 'admin':
			console.log("Welcome back to the portal " + role + '!');
			break;

		case 'teacher':
			console.log("Welcome back to the portal  " + role + '!');
			break;

		case 'student':
			console.log("Welcome back to the portal " + role + '!' );
			break;

		default:
			console.log("Role out of range.");

		}

	}

}

 login(username, password, role);



// -------------------------------------------------------------------------------


/*
    2. Create a function called checkAverage able to receive 4 numbers as arguments calculate its average and log a message for  the user about their letter equivalent in the console.
        -add parameters appropriate to describe the arguments.
        -create a new function scoped variable called average.
        -calculate the average of the 4 number inputs and store it in the variable average.
        -research the use of Math.round() and round off the value of the average variable.
            -update the average variable with the use of Math.round()
            -console.log() the average variable to check if it is rounding off first.
        -add an if statement to check if the value of average is less than or equal to 74.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is F"
        -add an else if statement to check if the value of average is greater than or equal to 75 and if average is less than or equal to 79.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is D"
        -add an else if statement to check if the value of average is greater than or equal to 80 and if average is less than or equal to 84.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is C"
        -add an else if statement to check if the value of average is greater than or equal to 85 and if average is less than or equal to 89.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is B"
        -add an else if statement to check if the value of average is greater than or equal to 90 and if average is less than or equal to 95.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is A"
        -add an else if statement to check if the value of average is greater than 96.
            -if it is, return the following message:
            "Hello, student, your average is <show average>. The letter equivalent is A+"

        Invoke and add a number as argument using the browser console.
    Note: strictly follow the instructed function names.
*/
// --------------------------------------------------------------------------
// input from user
let score1 = parseInt(prompt("What is your first score?: "));
let score2 = parseInt(prompt("What is your second score?: "));
let score3 = parseInt(prompt("What is your third score?: "));
let score4 = parseInt(prompt("What is your fourth score?: "));

let checkAverage = Math.round((score1 + score2 + score3 + score4)/4);

	if (checkAverage <= 74) { //
		console.log("Hello student, your average is: " + checkAverage + " The letter equivalent is F");
	}
	else if (checkAverage <= 79) { // 
		console.log("Hello student, your average is: " + checkAverage + " The letter equivalent is D");
	}
	else if (checkAverage <= 84) { // 
		console.log("Hello student, your average is: " + checkAverage + " The letter equivalent is C");
	}
	else if (checkAverage <= 89) { // 
		console.log("Hello student, your average is: " + checkAverage + " The letter equivalent is B");
	}
	else if (checkAverage <= 95) { // 
		console.log("Hello student, your average is: " + checkAverage + " The letter equivalent is A");
	}
	else if (checkAverage >= 96) { //
		console.log("Hello student, your average is: " + checkAverage + " The letter equivalent is A+");
	}


	// ---------------------------------------------------------------------------------------























//Do not modify
//For exporting to test.js
try {
    module.exports = {
       login, checkAverage
    }
} catch(err) {

}




