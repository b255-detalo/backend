// console.log("Hello World");

//Follow the property names and spelling given in the google slide instructions.

// Create an object called trainer using object literals

let trainer = {
	name: 'Ash Ketchum',
	age: 10,
	pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],

	friends: {

		hoenn: ['May', 'Max'],
		kanto: ['Brock', 'Misty'],
	}, 

	talked: function() {
		console.log(trainer.pokemon[0]+'!' + ' I choose you!');
	}
}
	console.log(trainer);

console.log("Result of dot notation:");
console.log(trainer.name);

console.log('Result of bracket notation:');
console.log(trainer.pokemon);

console.log('Result of talk method');

trainer.talked();

// ---------------------------

function Pokemon(name, level, health, attack)        {
		// Properties
		this.name = name;
		this.level = level;
		this.health = health;
		this.attack = attack;


		// Methods
		this.tackle = function(target){

			targetPokemonhealth = (target.health - level);


			console.log(this.name + ' tackled ' + target.name);
			console.log(target.name +"'s" + " health is reduced to " + targetPokemonhealth);

			if(targetPokemonhealth <= 0) {
				return target.faint();
			} else {
				return(target.name);
			}
			
		}

		this.faint = function(){
			console.log(this.name + ' fainted');
	    }

};  

// Prime
    let pikachu = new Pokemon("Pikachu", 12, 24, 12);
	let geodude = new Pokemon("Geodude", 8, 16, 8);
	let mewtwo = new Pokemon("Mewtwo", 100, 200, 100);
		console.log(pikachu);
		console.log(geodude);
		console.log(mewtwo);

// Attacked
	let pikachuFainted = new Pokemon("Pikachu", 12, 16, 12);
	let geodudeFainted = new Pokemon("Geodude", 8, -84, 8);
	

		geodude.tackle(pikachu);
		console.log(pikachuFainted);
		mewtwo.tackle(geodude);
		// geodude.faint();

		console.log(geodudeFainted);
		
		


		
		














// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method


// Create a constructor function called Pokemon for creating a pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Invoke the tackle method and target a different object


// Invoke the tackle method and target a different object
















































//Do not modify
//For exporting to test.js
try{
	module.exports = {
		trainer,
		Pokemon 
	}
} catch(err) {

}
