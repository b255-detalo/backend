console.log("Hello World!");
// Getting All Post
// The fetch API allows you to asynchronously request for a resource or data.
// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value.

console.log(fetch(`https://jsonplaceholder.typicode.com/posts`));
// Retrieves all post following the REST API
// By using the .then method we can now check for the status of the promise.
fetch(`https://jsonplaceholder.typicode.com/posts`)
// The "fetch" method will return a promise that resolves to a response captures the response object.
// The "then" method captures the response object and returns another promise which will be eventually resolve or rejected.
.then(response => console.log(response.status));
fetch("https://jsonplaceholder.typicode.com/posts")
// Use the "json" method from the "response" object to convert the data retrieve into JSON format to be used in our application
.then((response) => response.json())
// Print the converted JSON value from the fetch request
// Using multiple "then" method creates a "promise chain"
.then((json) => console.log(json));


// The "async" and "await" keywords are another approach that can be used to achieve asynchronous code.
// Used in functions to indicate which portions of code should be waited for
// Creates an asynchrounous function
async function fetchData(){
    // waits for the "fetch" methods to complete then stores gthe value in the result variable
    let result = await fetch('https://jsonplaceholder.typicode.com/posts');
    // Result returned by fetch returns as a promise
    console.log(result);
    // the returned response is an object
    console.log(typeof result);
    // We cannot access the content of response by directly accessing its body property
    console.log(result.body);

    // Converts the data from the "response" object as JSON
    let json =  await result.json();
    // Print out the content of the "response" object
    console.log(json);
}

fetchData();


// Gettinng a Specific Post
fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response) => response.json())
.then((json) => console.log(json));


// Creating a POST
fetch('https://jsonplaceholder.typicode.com/posts', {
    // Sets the method os the request object to POST following the  REST is get
        method: 'POST',
        // Sets the header data of the request object to be sent to the backend
        // Specified that the content will be in a JSON structure
        headers: {
            'Content-Type': 'application/json',
        },
        // Sets the content/body data of the "resquest" object to be sent to the backend
        // JSON.stringify converts the object data intoa stringify JSON
        body: JSON.stringify({
            title: 'New Post',
            body: 'hello World',
            userId: 1
        })
               
})
.then((response) => response.json())
.then((json) => console.log(json));


// Updating a post using PUT method
fetch('https://jsonplaceholder.typicode.com/posts/1', {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },

        body: JSON.stringify({
            id: 1,
            title: 'Updated Post',
            body: 'Hello Again',
            userId: 1
        })
               
})
.then((response) => response.json())
.then((json) => console.log(json));

// Updating a post using PATCH

// Updates a specific post following the REST API
// The difference between PUT and PATCH is the number of properties beiung changed.
// PATCH is used to update the whole object
// PUT is used to update a single/several properties.

fetch('https://jsonplaceholder.typicode.com/posts/1', {

    method: 'PUT',
    headers: {
        "content-Type": "application/json",
    },
    body: JSON.stringify({
        title: "Corrected post"
    })
})
.then((response) => response.json())
.then((json) => console.log(json));



// DELETING a post
// Deleting a specific post following the REST API
fetch('https://jsonplaceholder.typicode.com/posts/1',
{
    method: "DELETE"
});


// Filtering post
// The data can be filtered by sending the userID along with the URL
// Information sent via the URL can be done by adding the question mark symbol(?)

fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
.then((response) => response.json())
.then((json) => console.log(json));



// retrieving nested/related comments to post
// retrieving comments for a specific post following the REST API
fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response) => response.json())
.then((json) => console.log(json));
