
/*
	Note: strictly follow the variable names.
	
	1. Debug the following code to return the correct value and mimic the output.
*/

	let num1 = 25;
	let num2 = 5;
	console.log("The result of num1 + num2 should be 30.");
	console.log("Actual Result:");
	console.log(num1 + num2);

	let num3 = 156;
	let num4 = 44;
	console.log("The result of num3 + num4 should be 200.");
	console.log("Actual Result:");
	console.log(num3 + num4);

 
	let num5 = 17;
	let num6 = 10;
	console.log("The result of num5 - num6 should be 7.");
	console.log("Actual Result:");
	console.log(num5-num6);

// ---------------------------------------------------- 1 down

		
/*
   525600

	2. Given the values below, calculate the total number of minutes in a year and save the result in a variable called resultMinutes.

*/
	let minutesHour = 60;
	let hoursDay = 24;
	let daysWeek = 7;
	let weeksMonth = 4;
	let monthsYear = 12;
	let daysYear = 365;

	let resultMinutes = (minutesHour * hoursDay) * daysYear;
	let a = "There are ";
	let b = " minutes in a year.";
	let c = a + resultMinutes + b;
	console.log(c);


// There are 525600 minutes in a year. (Expected output)

// 1440 mins in a day, so 356 x 1440 = 525600

/* NOTES
1 second equals 0.0166667

60 seconds equals 1 minute.

60 minutes = 1 hour

1 day equals 1440 minutes.

1 week equals 10080 minutes.

1 month is 43800 minutes.

1 year equals 525600 minutes.
*/


// ---------------------------------------------------- 


/*
	3. Given the values below, calculate and convert the temperature from celsius to fahrenheit and save the result in a variable called resultFahrenheit.
*/ 

// THE FORMULA (celius * 9/5) + 32  
// (132 * 9 / 5) + 32

	let tempCelsius = 132;

	let A = 9;
	let B = 5;
	let C = 32;
	let D = tempCelsius * A/B + C;
	let E = " degrees Celsius when converted to Farenheit is ";
	let resultFahrenheit = (tempCelsius + E + D);
	console.log(resultFahrenheit);

// ---------------------------------------------------- 


/*
	4a. Given the values below, identify if the values of the following variable are divisible by 8.
	   -Use a modulo operator to identify the divisibility of the number to 8.
	   -Save the result of the operation in a variable called remainder1.
	   -Log the value of the remainder in the console.

	   -Using the strict equality operator, check if the remainder is equal to 0. 
	   -Save the returned value of the comparison in a variable called isDivisibleBy8
	   -Log a message in the console if num7 is divisible by 8.
	   -Log the value of isDivisibleBy8 in the console.

*/  

	//Log the value of the remainder in the console.
	//Log the value of isDivisibleBy8 in the console.

	let modulo8 = 8;
	let num7 = 165;
	let message1 = "The remainder of ";
	let message2 = " divided by ";
	let message3 = " is: ";
	let remainder1 = num7 % modulo8;
	let isDivisibleBy8 = (remainder1 === 0);

// LAHAT ng console.log nasa baba dapat para d mag error
	console.log(message1 + num7 + message2 + modulo8 + message3 + remainder1);
	console.log("Is num7 divisible by 8?");
	console.log(isDivisibleBy8);

// -------


// remainder1
// isDivisibleBy8
		/*
1.
	let XYZ = 0;
	let BB = 0;
	console.log(XYZ === BB);
	-------------

2.
	let XYZ = 0;
	let BB = 8;
	
	if(XYZ === 8){
		console.log("yes");
	}
	else {
		console.log("no");
	}

	*/

// ------------------------------------------------------------------

/*
	4b. Given the values below, identify if the values of the following variable are divisible by 4.
	   -Use a modulo operator to identify the divisibility of the number to 4.
	   -Save the result of the operation in a variable called remainder2.
	   -Log the value of the remainder in the console.
	   -Using the strict equality operator, check if the remainder is equal to 0. 
	   -Save the returned value of the comparison in a variable called isDivisibleBy4

	   -Log a message in the console if num8 is divisible by 4.
	   -Log the value of isDivisibleBy4 in the console.

*/

	let MOD = 0;
	let num8 = 348;

	let remainder2 = MOD % num8;
	let isDivisibleBy4 = (remainder2 === 0);
	let statement1 = "The ramainder of ";
	let statement2 = " devided by 4 is: ";

	console.log(statement1 + num8 + statement2 + remainder2);  // either remainder2 or MOD just to show 0
	console.log("Is num8 divisible by 4?");
	console.log(isDivisibleBy4);


//Log the value of the remainder in the console.
//Log the value of isDivisibleBy4 in the console.





//  - - - - - - - - - - - - - - - - - - - - - -  - - -
	//Do not modify
	//For exporting to test.js
	try{
		module.exports = {
			num1,num2,num3,num4,num5,num6,resultMinutes,
			resultFahrenheit,remainder1,remainder2,
			isDivisibleBy4,isDivisibleBy8
		}
	} catch(err){

	}





