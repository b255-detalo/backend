// [SECTION] Comparisson Operators

//  $gt/$gte
db.users.find({age: {$gt : 50}}).pretty(); // will find files with age greater than 50

db.users.find({age: {$gte : 76}}).pretty();// 76 is the baseline



// $lt/$lte Operator
db.users.find({age : {$lt : 50}});

db.users.find({age : {$lte : 50}});


// $ne Operator
// Allows us to find documents that have field number values not equal to a specified value.
db.users.find({age : {$ne : 82}});// will look for not equal docs


// $in Operator
// Allows us to find documents with specific match criteria one field using different values.
db.users.find({lastName: {$in: ["Hawking","Doe"]}});
db.courses.find({name: {$in: ["HTML 101","CSS 101"]}});


// Logocal Querry Operators
// $or Allows us to find documents that match a single criteria from multipole provided search criteria
db.users.find({$or: [{firstName: "Neil"},{age: 21}]});

db.users.find({$or: [{firstName: "Neil"},{age: {$gt: 70}}]});//nagpasok pa ng isang operator

// $and Operator
// Alows us to find documents matching multiple criteraia in a single field
db.users.find({ $and: [{age: {$ne: 82}}, {age: {$ne: 76}} ]});



// FIELD Projection
/* 
-Retrieving documents are common operatiuons that we do by default. MongoDb return the whole document as a response
- When dealing with complex data structures, there might be instances when fields are not useful for the query we are trying to accomplish
- Ti help with the readability of values return, we can include/exclude fields from tha response
*/

// Exclusion: 0, Inclusion: 1
db.users.find(
    {
        firstName: "Jane"
    },
    {
        firstName: 1,
        lastName: 1,
        email: 1
    }
);

db.users.find(
    {
        firstName: "Jane"
    },
    {   company: 0,
        email: 0
    }
);


// Supressing the ID field
/* 
-Allows us to exclude the "_id" field when retrieving documents
- When using field projection, field inclusion and exlcusion may not be used at the same time.
-Excluding the "_id" field is the only exception to this rule
*/

db.users.find(
    {
        firstName: "Jane"
    },
    {   firstName: 1,
        lastName: 1,
        company: 0,
        _id: 0
    }
);


db.users.insert({
    firstName: "John",
    lastName: "Smith",
    age: 21,
    contact: {
        phone: "87654321",
        email: "johnsmith@gmail.com"
    },
    courses: [ "CSS", "Javascript", "Python" ],
    department: "none"
});


db.users.find(
    {
        firstName: "John"
    },
    {   firstName: 1,
        lastName: 1,
        "contact.phone": 1
    }
);

// Supressing sped=cific filelds in embedded documents 

db.users.find(
    {
        firstName: "John"
    },
    {  
        "contact.phone": 0
    }
);


db.users.insert({
    namearr: [
        {
            namea: "juan"
        },
        {
            nameb: "tamad"
        }
    ]
});

// project specific array elements in the returned arrahy 
// The $slice operator allows us to retrieve only 1 element that ,matched the search criteria

db.users.find(
    {
        "namearr":
        {
            namea: "juan"
        }
    },
        {
            namearr: {$slice: 1}
        }
);

// Evaluation Querry Operators
// $regex Operator
/* 
-Allwos to find documents that match a specific string pattern using regular expressions
*/

// Case sensitive querry
db.users.find({firstName: {$regex: 'N'}});


// Case insensitive query
db.users.find({firstName: {$regex: 'j', $options: '$i'}});