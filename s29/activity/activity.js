

// 1. Find users with letter s in their first name or d in their last name.
// - Use the $or operator.
// - Show only the firstName and lastName fields and hide the _id field.
db.users.find({ 
    $or: [
        {firstName: {$regex: 's', $options: '$i'}},
        {lastName: {$regex: 'd', $options: '$i'}}, 
        ] 
    }, 
    
        {"firstName":1,"lastName":1,_id:0}//Displaying only this fields, _id is 0.
    
    );

        
/* 
        db.users.find({},{"firstName":1,"lastName":1,_id:0});

        db.users.find({},{"age":1,_id:0},{"firstName":"Jane"},
        {"firstName":"Stephen"}); */











// --------------------------------------
// 2. Find users who have none in the company property and their age is greater than or equal to 70.
// - Use the $and operator
db.users.find({ $and: [{company: "none"}, {age : {$gte: 70}}]})

// db.users.find({age : { $gte : 50}}).pretty();




// -------------------------------------------------
/*  3. Find users with the letter e in their first name and has an age of less than or equal to 30.
- Use the $and, $regex and $lte operators */

db.users.find({ $and: 
    [
        {firstName: {$regex: 'e', $options: '$e'}}, 
        {age : {$lte: 30}}
    ]
});