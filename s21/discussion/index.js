console.log("Test!");


// An array in prormaing is simply a list of data

let studentNumbers = ['2020-1923', '2020-1924', '2020-1925']

// [SECTION] Arrays
/*
	Arrays are use d to store multiuple related values ina single variable
	- They are declared using squared brackets[] alos known as "Array Literals"
	-Commonly used to store numerous amounts of data to manipulates in order  to perform a number of tasks

*/

// Common examples of arrays
let grades = [98.5, 94.3, 89.2];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox'];

// Possible use of array but is not recommended
let mixedArr = [12, 'Asus', null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);

// Alternative way towrite arrays
let myTasks = [

	'drink html',
	'eat javascript',
	'inhale css',
	'bake sass'

	];

// Creating an array wih values from variables
let city1 = 'Tokyo';
let city2 = 'Manila';
let city3 = 'Jakarta';

let cities = [city1, city2, city3];

console.log(myTasks);
console.log(cities);


// [SECTION] Lenght property

// The ,lenght property allows us to get and set the total number of items in an array

console.log(myTasks.length);
console.log(cities.length);

// length property can also be used w/ strings. some array methods and properties can also be used w/ strings.

let fullname = 'Jaime Noble';
console.log(fullname.length);

// Length property can also set the totral number of item in an array, meaning we can actually delete the last item in the array.



myTasks.length = myTasks.length-1;
console.log(myTasks.length);
console.log(myTasks);

// To delete a specific item in an array we can employ array method(Wich will be shown in the next session)
// Anothger example of using decrement
cities.length--;
console.log(cities);

fullname.length = fullname.length-1;
console.log(fullname.length);
fullname.length--;
console.log(fullname);


// If you can shorten the array by setting the length property you can also lengthen it by adding a number into the length property.

let theBeatles = ['John', 'Paul', 'Ringo', 'George'];
theBeatles.length++;
console.log(theBeatles);

// READING FROM ARRAY
/*
	Accessing array elements is one of the more common tasks that we do with an array
	- This can be done through the use of arrayt indexes
	- each element in an array is associated with it's own index/number
	- In javaScript, the first element is associated w/ the number 0 and increasing this number by 1 for every proceeding element
	- the reason an array starts with 0 is due to how the language is design
	- Array indexes actually refer to an address/location in the devices memory and how the information is stored
	- Example array location in memory
		 Array[0] = 0X7ffe947bad0
*/
console.log(grades[0]);
console.log(computerBrands[0]);

// Accessing an array elemet that does not exist will return 'undefined'
console.log(grades[20]);


let lakersLegends = ['Kobe', 'Shaq', 'Lebron', 'Magic', 'Kareem'];
console.log(lakersLegends[1]);
console.log(lakersLegends[3]);

// You can save/store array items in another  variable
let currenLaker = lakersLegends[2];
console.log(currenLaker);

// You can also re-assign array values using the items's idices

console.log('Array before reassignment');
console.log(lakersLegends);
lakersLegends[2] = 'Pau Gasol';
console.log("Array after reassigment");
console.log(lakersLegends);


let bullsLegends = ['Jordan', 'Pippen', 'Rodman', 'Rose', 'Kukoc'];
let lastElementIndex = bullsLegends.length-1;
console.log(bullsLegends[lastElementIndex]);

// You can also add it directly
console.log(bullsLegends[bullsLegends.length-1]);


// Adding items into an array
let newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);

console.log(newArr[1]);
newArr[1] = "Tifa Lockhart";
console.log(newArr);

// You can also add items at the end of the array./ Replace Tifa Lockhart
// newArr[newArr.length-1] = "Aerith Gainsborough";

newArr[newArr.length] = "Barret Wallace";//adding another
console.log(newArr);

// Looping over an array
// You can use a for loop to iterate over all items in an array
// Set the counter as the index and set a condition that as long as the current index iterated is less than a =the length of the array

for (let index =0; index<newArr.length; index++) {
	console.log(newArr[index]);
}


let numArr = [5,12,30,46,40];

for (let index =0; index< numArr.length; index++) {

	if(numArr[index] %5 === 0){
		console.log(numArr[index] + " is divisible by 5");
		
	}
	else{
			console.log(numArr[index] + " is not divisible by 5");
		}
}

// Multi-Dimensional Array
/*
	these arrays are useful for storing compolex data structures
	- practical application of this is to help visualize /create real world objecyts
*/

let chessBoard = [
	['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'h1'],
	['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'h2'],
	['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'h3'],
	['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'h4'],
	['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'h5'],
	['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'h6'],
	['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'h7'],
	['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'h8']

	]
console.log(chessBoard);

// Accessing multidimensional array
console.log(chessBoard[1][4]);

console.log("Pawn moves to: " + chessBoard[1][5]);