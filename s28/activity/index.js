db.rooms.insertOne({
    "name": "single",
    "accomodatate": 2,
    "price": 1000,
    "description": "A simple room with all the basic necessities",
    "roomsAvailable": 10,
    "isAvailable": false
});



db.rooms.insertMany([
    
    {
    "name": "double",
    "accomodatate": 3,
    "price": 2000,
    "description": "A room fit for small family going on a vacation",
    "roomsAvailable": 5,
    "isAvailable": false
},

    {
    "name": "queen",
    "accomodatate": 4,
    "price": 4000,
    "description": "A room with a queen sized bed perfect for a simple gateway",
    "roomsAvailable": 15,
    "isAvailable": false
}

]);


/* db.rooms.insertMany([

{
    "name": "single",
    "accomodatate": 2,
    "price": 1000,
    "description": "A simple room with all the basic necessities",
    "roomsAvailable": 10,
    "isAvailable": false
},

{
    "name": "double",
    "accomodatate": 3,
    "price": 2000,
    "description": "A room fit for small family going on a vacation",
    "roomsAvailable": 5,
    "isAvailable": false
},



    {
    "name": "queen",
    "accomodatate": 4,
    "price": 4000,
    "description": "A room with a queen sized bed perfect for a simple gateway",
    "roomsAvailable": 15,
    "isAvailable": false
}
]);
 */



db.rooms.find({
    "name": "double"
});




db.rooms.updateOne(
    {
        "name": "queen"
    },

    {
        $set:{
            "roomsAvailable": 0
        }
    }
);



db.rooms.deleteMany({
    "roomsAvailable": 0
});