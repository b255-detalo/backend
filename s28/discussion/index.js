// CRUD OPERATIONS
//Insert Document (CREATE)

/*  Syntax:
 Insert One Document
 db.collectionName.insertOne({
    "fieldA": "valueA",
    "fieldB": "valueB",

 });

 Insert many Documents
 db.collectionName.insertMany({
    {
        "fieldA": "valueA",
         "fieldB": "valueB",
    },
     {
        "fieldA": "valueA",
         "fieldB": "valueB",
    }
 });

 */
db.users.insertOne({
    "firstName": "Jane",
    "lastName": "Doe",
    "age": 21,
    "email": "janedoe@mail.com",
    "company": "none"
});

db.users.insertMany({
    {
        "firstName": "Stephen",
        "lastName": "Hawking",
        "age": 76,
        "email": "stephenhawking@mail.com",
        "company": "none"
    },

    {
        "firstName": "Neil",
        "lastName": "Armstrong",
        "age": 82,
        "email": "sneilarmstrong@mail.com",
        "company": "none"
    }
});

//Mini Activity


db.courses.insertMany([
    {
        "name": "Javascript101",
        "price": 5000,
        "description":"Introduction to Javascript",
        "isActive": true,
    },
    {
        "name": "HTML 101",
        "price": 2000,
        "description":"Introduction to HTML",
        "isActive": true,
    },
    {
        "name": "CSS 101",
        "price": 2500,
        "description":"Introduction to CSS",
        "isActive": true,
    }
]);



// Find Documents(Read/Retrieve)

db.users.find();//This will retrieve all the documents in the collection

db.users.find({
    "firstName": "Jane"
});

// specific
db.users.find({
    "firstName": "Neil",
    "age": 82
});


db.users.findOne({});//returns the first document in our collection
db.users.findOne({
    "firstName": "Stephen"

});


// Update Documents
db.users.insertOne(
    
        "firstName": "Test",
        "lastName": "Test",
        "age": 0,
        "email": "test@mail.com",
        "company": "none"
    
);

// Updating One document
db.users.updateOne(
    {
        "firstName": "Bill"
    },

    {
        $set:{
            "firstName": "Bill",
            "lastName": "Gates",
            "age": 65,
            "email": "billgates@mail.com",
            "department": "operations",
            "status": "active"
        }
    }
);

// Removing a Field

db.users.updateOne(
    {
        "firstname": "Bill"
    },

    {
        $unset:{
            "status": "active"
        }
    }
);

// Updating Multiple Documents
db.users.updatemany(
    {
        "company": "none"
    },
    {
        $set:{
            "company": "HR"
        }
    }
);

db.users.updateOne(
    {},
    {
        $set:{
            "company": "operations"
        }
    }
);

db.users.updatemany(
    {},
    {
        $set:{
            "company": "comp"
        }
    }
);

// DELETING DOCUMENTS

db.users.insertOne({
    "firstName": "Test"
});

db.users.deleteOne({
    "firstName": "Test"
});


db.users.deleteMany({ //will delete all with comp
    "company": "comp"
});

db.courses.deleteMany({});