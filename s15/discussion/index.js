//  [SECTION] Syntax, Statements and Comments
// Statements in programming are instructions that we tell the computer to perform
// JS statements usually ends with the semicolon (;)
console.log("Hello World!")

// Comments are parts of the codes that get egnored by the language
// Comments are meant to describe the written code

/*
There are 2 types of comments:
1. The single-line comment denoted by two slashes
2. The multi-line comment denoted by slash

*/

// [SECTION] Variables
// It is used to contain
// Any informnation that is used by an application is store in what we call a memory
// When we create variables certain portions of a devices is given "name" that we call "Variables"


// Declaring Variables
// Declaring VAriables tells our device that a variable name is created and ready to store data
// Declaring a variable woithout assigning a value will automatically give it the value of "undefine"


let myVariable;
console.log(myVariable);

// console.log() is useful for printing out values of variables or certain results of code into the console.

let hello;
console.log(hello);

// Variables must be declared before they are used
// Using variables before they're declared will return an error

/*
 Guides in writing variables
 1. use the 'let keyword' followed by the variable name of your choosing and use the asignment operator (=) to assign a value.
 2. Variable names should start with a lowercase character, use camelCase for multiple words.
 3. For Constant Variables use the 'const' keyword.
 4.Variable names should be indicative (or descriptive) of the value stored to avoid confusion.

*/

// Declaring and initializing Variables
// Initializing variables -  the instance when a variable is given it's initial/starting value

let productName = "desktop computer";
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

// in the context of certain applications, some variables/information are constant and should not be changed.

// In this example, the interest rate for a loan, savings account or mortgage must not be changed due to real world concerns.

const interest = 2.539;

// reassigning variable values
// Reaassigning a variable means changing it's initial or previuos value into another value

// pag walang let means re-assigning
productName = 'Laptop';
console.log(productName);


// re-aasigning variables vs initializing variables
// declares a variable first
let supplier;
// Initialization is done after the variable has been declared
// this is considered as initialization because it is the 1st time the value has been assigned to variable

supplier = "John Smith Tradings";
console.log(supplier);

// This is considered a reassignment because it's initial value was already declared
supplier = "Zuitt Store"
console.log(supplier);

/*const pi;
p = 3.14;
console.log(pi); */

// var vs. let/const
   // var is also used in declaring variables. but var is an ECMAScript (ES1) feature (JavaScript 1997)
// let/const was introduced as new feature in ES6(2015)

// There are issues associated w/ variables declared with var, reagarding hoisting.
// Hoisting in JavaScripts default behaviour of moving declarations to the top
// In terms of variables and constants, keyword var is hoisted and let and const does not allow hoisting.

// for example:
a = 5;
console.log(a);
var a;

// in the above ex: variable a is used before declaring it. and the program works and displays the output of 5

// let/const local/global scope
// Scope essentially means where these variables are available for use
// let and const are block scoped
// a block is chunk of code bounded bt a {} A block lives in curly braces. Anything within curly braces is a block.


// Multiple variable declarations
// Multiple variables maybe declared in one line
// thouhgh it is quicker to do without having to re-type the "let" keyword, it is still best practice to use multiple "let/const" keywords

let productCode = 'DC017', productBrand = "Dell";
console.log(productCode, productBrand);

// Data types

// STRINGS are a series of characters that create a word, a phrase, a sentence, or anything related to creating text
// Strings in JavaScriopt can be written either single ('') or double("") quote

let country = 'Philippines';
let province = "Metro Manila";

// Concatenating Strings
// Multiple string values can be combined to create a single string using the "+" symbol

let fullAddress = province + ',' + country;
console.log(fullAddress);

let greeting = 'I live in the ' +country;
console.log(greeting);


// The escape char 
let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);

let message = "John's employees went home early";
console.log(message);

message = 'John\'s employees went home early';
console.log(message);

// Numbers
// Integers/Whole Numbers dont use '' or " "
let headCount = 26;
console.log(headCount);

// Decimal Numbers/Fractions
let grade = 98.7;
console.log(grade);


// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining text and strings
console.log("John's grade last quarter is " + grade);

// Boolean 
// Boolean values are normally used to store values relating to the state of certain tings
// This wiull usefull in further discussion abot creating logic to make our applications respond to certain scenarios

let isMarried = false;
let inGoodConduct = true;
console.log("isMarried: " + isMarried);
console.log("inGoodConduct: " + inGoodConduct);

// Arrays
// are a special kind of data type that's used to store multiple values
// Arrays can store diff. data tyupes but is normally store similar data types

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);


// different data types
let details = ["John", "Smith", 32, true];
console.log(details);

// Objects are another special kind of data type thats use to mimic real worl objects/items
// They are used to complex data that contains pieces of information that are reklevant to each other
// Every individual piece of info is called a property of the obj.
let person = {
	fullName: 'Juan Dela Cruz',
	age: 35,
	isMarried: false,
	contact: ["+63917 123 4567", "8123 4567"],
	address:{
	houseNumber: '345',
	city: 'Manila'
	}
}
console.log(person);
// they're also useful for creating abstract objects
let myGrades = {
	firstGrading: 98.7,
	secondGrading: 92.1,
	thirdGrading: 90.2,
	fourthGrading: 94.6
}
console.log(myGrades);

// type of operator is used to determine the type of data or the value of avariable.
console.log(typeof myGrades);

const anime = ['one piece', 'one punch man', 'attack on titan'];
console.log(anime[0]);

anime[0] = ['kimetsu no yaiba'];
console.log(anime);

// Null is used to intentionally express the absence of a value in a variable declaration/initializatio

let spouse = null;
// Using null compared to a 0 value is much better for readability purposes
// nulkl is also considered to be a data type of its own compared to 0 w/c a data type of number

// Undefined
// Represents the state os a variable that has been declared but w/out an asignment
let fullName;
console.log(fullName);

// Undefined vs. Null
// One clear difference between undefined and null is that fr undefined, a variable was created