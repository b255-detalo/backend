const express = require("express");
// Mongoose is a package that allows creation of Schemas to model our data structures
// Also has access to a number of methods for manipulating our database
const mongoose = require("mongoose");

const app = express();
const port = 4000;

// [SECTION] MongoDB connection
mongoose.connect("mongodb+srv://apollo-255:admin123@zuitt-bootcamp.dckhweo.mongodb.net/?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}
);

//Set notifications for connection success or failure
//Connection to the database
//Allows to handle errors when the initial connection is established
//Works with the on and once mongoose methods 
let db = mongoose.connection
// If a connection error occured, output in the console
// console.error.bind(console) allows us to print errors in the browser console and in the terminal
db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"))

// [SECTION] Mongoose Schemas
// Schemas determining the structure of the documents to be written in the database
// Schemas act as a blueprint to our data
// use the Schema() constructor of the mongoose module to create a new Schema Object
const taskSchema = new mongoose.Schema({
	// Define the fields with the corresponding data type
	// For a task, it needs a "task name" and "task status"
	// There is a field called "name" and its data type is "String"
	name: String,
	status: {
		type: String,
		default: "pending"
	}
})

// [SECTION] Models
// Uses schemas and are used to create/instantiate objects that correspond to the schema
// Models use Schemas and they act as the middleman from the server(JS code) to our database
// Server > Schema(blueprint) > Database > Collection

// The variable/object "Task" and "User" can now be used to run commands for interacting with our database
// "Task" and "User" are both capitalized following the MVC approach for naming conventions
// Models must be in singular form and capitalized
// The first parameter of the Mongoose model method indicates the collection in where to store data
// The second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection

const Task = mongoose.model("Task", taskSchema);

// [SECTION] Creation of todo list routes
// Setup for allowing the server to handle data from requests
// Allows your app to read json data
app.use(express.json());
// Allows your app to read data from forms
app.use(express.urlencoded({extended:true}));

app.post("/tasks", (req, res)=> {
	
	// Check if there are duplicate tasks
	// "findOne" is a Mongoose method that acts similar to "find" of MongoDB
	// findOne() returns the first document that matches the search criteria as a single object.
	// findOne() can send the possible result or error in another method called then() for further processing.
	// .then() is chained to another method that is able to return a value or an error.
	// .then() waits for the previous method to complete its process. It will only run once the previous method is able to return a value or error.
	// .then() method can then process the returned result or error in a callback method inside it.
	// the callback method in the then() will be able to receive the result or error returned by the previous method it is attached to.
	// the .findOne() method returns the result first and the error second as parameters.
	// Call back functions in mongoose methods are programmed this way to store the returned results in the first parameter and any errors in the second parameter
	// If there are no matches, the value of result is null
	// "err" is a shorthand naming convention for errors
	Task.findOne({name : req.body.name}).then((result, err) => {
		// If a document was found and the document's name matches the information sent via the client/Postman
		if(result != null && result.name == req.body.name){
			// Return a message to the client/Postman
			return res.send("Duplicate task found");

		// If no document was found
		} else {

			// Create a new task and save it to the database
			let newTask = new Task({
				name : req.body.name
			});

			// The "save" method will store the information to the database
			// Since the "newTask" was created/instantiated from the Mongoose Schema it will gain access to this method to save to the database
			// The "save()" method can send the result or error in another JS method called then()
			// the .save() method returns the result first and the error second as parameters.
			newTask.save().then((savedTask, saveErr) => {
				// If there are errors in saving
				if(saveErr){

					// Will print any errors found in the console
					// saveErr is an error object that will contain details about the error
					// Errors normally come as an object data type
					return console.error(saveErr);

				// No error found while creating the document
				} else {

					// Return a status code of 201 for created
					// Sends a message "New task created" on successful creation
					return res.status(201).send("New task created");

				}
			})
		}

	})
})

app.get("/tasks", (req, res) => {
    Task.find({}).then((result, err) => {
        if(err) {
            return console.log(err)
        }
        else {
            return res.status(200).json ({
                data: result
            })
        }
    })
})






















// Listen to the port, meaning, if the port is accessed we run the server
if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));

}

module.exports = app;