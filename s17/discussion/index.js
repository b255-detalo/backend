console.log("Hello!");

// Functions in javascript are lines /blocks of codes that tell our device/applicatiuon to perform a certain task


// Function Declaration
function printName(){

	console.log("My name is John");
};

// Function invocation
printName();


// Function declaration vs. expression
	// functiuon declaration
	// A function can be created through function declaration by using a keyword and adding name
declaredFunction();

function declaredFunction(){
	console.log("Hello world declaredFunction()");
}

declaredFunction();

// Function Expression
// A function can also be stored in a variable this is called a Function Expression
// A function expression is a annoimous function assign to the variableFunction

// Anonymous Function is a function w/out a name
let variableFunction = function(){
	console.log("Hello Again!");
};

variableFunction();

// We can also create a function expression of a named function
// HOwever, to invoke trhe function expression, we invoke it by its variable name, not by its function name

let FuncExpression = function funcName(){

	console.log("Hello from the other side");

	FuncExpression();
}


// You can reassign declared functions and function, expresions to new anonymous functions

declarationFunction = function(){
	console.log("updated declaration");
};
declarationFunction();

funcExpression = function(){
	console.log("Updated Function Expression");
}

funcExpression();



// However we cannot re-assign a fuuntion expression initialized with const

const constantFunc = function(){

	console.log("Initialized with const!");

}

constantFunc();



// Function Scoping

// scope is a accessobility (visibility) of variables within our program



{
	let localVar = "Armando"

}

let globalVar = "Mr Worldwide";

console.log(globalVar);


// Function Scoop
function showNames(){

	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
}

showNames();



// NESTED FUNCTIONS

  //  You can create another function inside anopther function. this is called NESTED FUNCTION.

  function myNewFunction(){

  	let name = "Jane";

	  	function nestedFunction(){
	  		let nestedName = "John"
	  		console.log(name);
	  	}

  	nestedFunction();
  }

  myNewFunction();




  // Function and Global Scope Variables

   // Global scoped variables

		  let globalName = "Alexandro";

		  function myNewFunction2(){
		  	let nameInside = "Renz";

		  	//console.log(globalName);
		  }

		  myNewFunction2();


//  Using Alert

	 //  alert() allows us to shos small window at the top of our browser page to show information to our users. as proposed to console.log()

	alert("Hello World!"); // will run until met

		  function showSampleAlert(){
		  	alert("Hello, user!"); // saka mag sshow on 2nd click
		  }

		  showSampleAlert();

		  console.log("I will only log in the console when the alert is dismissed");

// Using promt
		// promt() allows su to shjow a small window at the browser to hather user input.
		  // it, much like alert() will have the page wairt=t until it is dismissed

		 let samplePrompt = prompt("Enter your name");
		 console.log("Hello " + samplePrompt);

let sampleNullPrompt = prompt("Dont enter anything");

console.log(sampleNullPrompt);


// Return an empty string when there is no input
// or null if the user cancels the prompt
 function printWelcomMesage(){
 	let firstName = prompt("Enter your first name: ");
 	let lastName = prompt("Enter your last name: ");

 	console.log("Hello " + firstName + " " + lastName + "!");
 	console.log("Welcome to my page!");

 };

 printWelcomMesage();



 // FUNCTION NAMING CONVENTION

// Function names should be definitive of the task it will perform. it usually contains a verb

 function getCourses(){
 	let courses = ["Science 101", "Math 101", "English 101"];
 	console.log(courses);
 }

 getCourses();

 //  Avoid generic names to avoid confusion

 function get(){
 	let name = "Jaime";
 	console.log(name);
 }
get();

// Avoid pointless and inappropriate function names 
function foo() {
	console.log(25%5);
}
foo();


// Name your functions in small caps. folow camelCase when naming variables and functions

 function displayCarInfo(){
	console.log("Brand: Toyota");
	console.log("Type: Sedan");
	console.log("Price: 1,500,000");
}
displayCarInfo();






