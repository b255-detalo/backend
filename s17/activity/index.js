/*
	//Note: strictly follow the variable names and function names.

	1. Create a function named printUserInfo() which is able to display a user's to fullname, age, location and other information.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
*/
	//first function here:

// PROMPT PALA HAHAH take 2



 function printUserInfo(){

 	let firstName = prompt("Enter your first name: ");
 	let lastName = prompt("Enter your last name: ");
 	let age = prompt("Enter your age: ");
 	let address = prompt("Enter your address: ");
 	let cat = prompt("Enter your the name of your cat: ");
 	let dog = prompt("Enter your the name of your dog: ");


 	console.log("Hello, I'm " + firstName + " " + lastName );
 	console.log("I am " + age + " years old." );
 	console.log("I live in " + address );
 	console.log("I have a cat named " + cat );
 	console.log("I have a dog named " + dog );


 };

 printUserInfo();





// ----------------------------------------------------------------

/*


	2. Create a function named printFiveBands which is able to display 5 bands/musical artists.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/
	//second function here:


	function printFiveBands() {

		let beatles = "The Beatles";
		let taylor = "Taylor Swift";
		let eagles = "The Eagles";
		let rivermaya = "Rivermaya";
		let eraserheads = "Eraserheads";
		
		console.log(beatles);
		console.log(taylor);
		console.log(eagles);
		console.log(rivermaya);
		console.log(eraserheads);

	}

	printFiveBands();

// ------------------------------------------------------------------------------


/*
	3. Create a function named printFiveMovies which is able to display the name of 5 movies.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/
	
	//third function here:

function printFiveMovies() {

		let movie1 = "Lion King";
		let movie2 = "Howl's Moving Castle";
		let movie3 = "Meet The Robinsons";
		let movie4 = "School of Rock";
		let movie5 = "Sprited Away";
		

console.log(undefined); // just for adding space
		console.log(movie1);
		console.log(movie2);
		console.log(movie3);
		console.log(movie4);
		console.log(movie5);

	}

	printFiveMovies();

// ------------------------------------------------------------------------------


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
		-check your spelling

		-invoke the function to display information similar to the expected output in the console.
*/

function printFriends(){
	let friend1 = "Eugene"; 
	let friend2 = "Dennis"; 
	let friend3 = "Vincent";

console.log(undefined);
	console.log("These are my friends:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

	// console.log(friend1); 
	// console.log(friend2);




































// ------------------------------------------------------------------------

//Do not modify
//For exporting to test.js
try{
	module.exports = {
		printUserInfo,
		printFiveBands,
		printFiveMovies,
		printFriends
	}
} catch(err){

}
