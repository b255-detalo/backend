// Exponent Operator
// Template Literals

getCube = 2 ** 3;


message = `The cube of 2 is ${getCube}`
console.log(`The cube of ${message}`)

// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];


console.log(`I live at ${address[0]} ${address[1]}, ${address[2]} ${address[3]}`);




// Object Destructuring
const animal = {
	       name: "Lolong",
	    species: "saltwater crocodile",
	     weight: "1075 kgs",
	measurement: "20 ft 3 in"
}


console.log(`${animal.name} was a ${animal.species}. He weight at ${animal.weight} with a measurement of ${animal.measurement}.`);



// Arrow Functions
let numbers = [1, 2, 3, 4, 5, 15];


numbers.forEach((numbers) => 	{
	console.log(`${numbers}`)
})



// Javascript Classes

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;

	}
}
 const doggie = new Dog();



doggie.name = 'Frankie';
doggie.age = 5;
doggie.breed = 'Miniature Dachshund';
console.log(doggie);












//Do not modify
//For exporting to test.js
try {
	module.exports = {
		getCube,
		houseNumber,
		street,
		state,
		zipCode,
		name,
		species,
		weight,
		measurement,
		reduceNumber,
		Dog
	}	
} catch (err){

}
