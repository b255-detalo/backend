const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");//path folders
const auth = require("../auth");

// Route for checking if the user's email already exist in the database
// Invokes the checkEmailExists function froom the controller file to communicate with our database
// Passes the "body" property of our "requuest" object to the corresponding controller function
router.post("/checkEmail", (req, res) => {
    userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

// Route for user registration
router.post("/userRegistration", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Route for user authentication
router.post("/userLogInAuthentication", (req, res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});



// ------------------------
// Route for retrieving user details
router.get("/retrieveUserDetails", auth.verify, (req, res) => {
    // Uses the "decode" method defined in the "auth.js" file to retrieve user informatoin from the token pasing the "token" from the request header as an argument
    const userData = auth.decode(req.headers.authorization);

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : req.body.id}).then(resultFromController => res.send(resultFromController));

});


// Routes for enrolling user to a course
router.post("/enroll", (req, res) => {
	let data = {
		userId: req.body.userId,
		courseId: req.body.courseId
	}
	userController.enroll(data).then(resultFromController => res.send(resultFromController));
})



// -------------------- ACTIVITY s41------
router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		// User ID will be retrieved from the request header
		userId: auth.decode(req.headers.authorization).id,
		// course ID will be retrieved from the request body
		courseId: req.body.courseId
	}
	userController.enroll(data).then(resultFromController => res.send(resultFromController));
});

// Routes for enrolling user to a course
router.post("/NonAdminUserCheckout", (req, res) => {
	let data = {
		userId: req.body.userId,
		courseId: req.body.courseId
	}
	userController.enroll(data).then(resultFromController => res.send(resultFromController));
})


















// Allows us to export the "router" object that will be accessd in the our "index.js" file
module.exports = router;