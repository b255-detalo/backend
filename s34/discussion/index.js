
// Use the "require" directive to load the express module/package
// A "module" is a software component or a part of a program that contains one or more routines
// This is used to get the contents of the expres package to be used by our application
// It also allows us to access to methods and functions that will allow us to create a server.
const express = require('express');
const { init } = require('express/lib/application');

// Create an application using express
// This creates an express application and stores this in a constant called app
// In laymans term app is our server
const app =  express()
// For our application server to run, we need a port to listen 
const port = 4000;

// Set up for allowing the server to handle data from requests
// Allows your app to read json data
// Methods used from express JS are middlewares
// Middleware is a software thagt provides comon services and capabilities and applicatoins outside of what's offered by the operating system.
app.use(express.json());
// Allows your app to read data from forms
// By default, information received from the URL can only be receive as a string or an array.
// By applying the option of "extended:true" this allows us to receive information in other data types such as an object which will use throut the our application.
app.use(express.urlencoded({extended:true}));

// ROUTES
// Express has a methods of corres[ponding to each HTTP method

app.get("/",(req,res) => {
    res.send("hello world")
});

// miniActivity, Create another GET route
// Our route dmust be named "hello"
// it mus output this message "Hello from the /hello endpoint"

app.get("/hello",(req,res) => {
    res.send("Hello from the /hello endpoint")
}); // Correct 

// Create a POST route
app.post("/hello", (req,res) => {
    // req.body contains the contents/data of the request body
    // All the properties defined in our Postman request will be accessible here as properties with the same names
    res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`)
})

// An array that will store user objects when the "/signup" route is accessed
let users = [];

app.post("/signup", (req,res) => {
    console.log(req.body);
    if(req.body.userName !== '' && req.body.password !== ''){
        users.push(req.body);
        res.send(`User ${req.body.userName} successfully registered!`);   
    } 
    else{
        res.send("Please input BOTH username and password")
    }
             
});

// Create PUT route to change password of a specific user
app.put("/change-password", (req,res) => {
    let message;
// Creates a for loop that will loop through element of an array
    for(let i=0; i<users.length; i++) {

// If the username provided in the postman client and the username of the current object in the loop is the same
        if(req.body.userName == users[i].userName){
            // Changes password of the user found by the loop
            users[i].password =req.body.password;

            message = `User ${req.body.userName}'s password has been updated`;
            // Breaks out of the loop once a user that matches the username provided is found
            break;
        } 
        else{
            message = "User does not exist."  
        }
    }
    res.send(message);
    console.log(users);
})
/* npm initnpm 
install express 
then git ignore*/



// if(require.main) would allow us to listen to the app directly if is not imported to another module, it will run the app directly.
// else, if it is needed to be imported, it will not run the app and instead export it to be used in another file.
if(require.main === module){
    app.listen(port, () => console.log(`Server running at port ${port}`))
}
module.exports = app;
