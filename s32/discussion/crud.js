let http = require('http');
let directory = [
    {
        "name": "Brandon",
        "email": "brandon@mail.com"
    },
    {
        "name": "Jobert",
        "email": "bjobert@mail.com"
    }
]

http.createServer(function(request, response){

    // route for returning all items upon receiving a GET request
    if(request.url == "/users" && request.method == "GET"){
            response.writeHead(200,{"Content-Type": "application/json"});

            // Input HAS to be data type STRING hence the JSON.stringify() mehod
            // This string input will be converted to desired output data type which been sent to JSON.
            // This is done because request and responses sent between client and node.js server requires thge information sent and received as a stringify JSON
            response.write(JSON.stringify(directory));
            response.end();
    }

    // A request object contsins several parts
    // -Headers - contains information abot the request context/content like what is the data type
    // -Body - contains the actual information being sent with the request.
    if(request.url == "/users" && request.method == "POST"){

            let requestBody = '';


        
request.on('data', function(data){

                // Assigns the data retrieved from the data stream to the request body
                    requestBody += data;
                })
 request.on('end', function()  {
            // We need this to be of data type JSON to access its properties 
            console.log(typeof requestBody);
            // converts the string requestBody to JSON
            requestBody = JSON.parse(requestBody);

            // Create a new object representing the new mock database record
            let newUser = {
                "name": requestBody.name,
                "email": requestBody.email
            }
            directory.push(newUser);
            console.log(directory);

            response.writeHead(200,{'Content-Type': 'application/json'});
            response.write(JSON.stringify(newUser));
            response.end();


    })
            
    }

}).listen(4000);

console.log('Server is running at localhost:4000');