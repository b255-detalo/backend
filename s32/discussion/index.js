
// Use the require durective to load a Node.js modules
// A "module" is a software component or part of a program that contains one or more routines
// the "HTTP module" lets node.js transfer data using the hypertext transfer protocol
// HTTP is a protocol that allows the fetching of resoiurces suych as html documents
// Clients(browser) and servers (nodejs/express js applications) communicate by exchanging individual messgaes.
// The mesgae sent 
let http = require('http');

// using this module's createServer() method, we can create an http server tha listens to requests on the specified port and gives response.
// The http modules has a createServer() method that accepts a function as an argument and allows for a creation of a server

http.createServer(function(request, response){

    // The HTTP method of the incoming request can be accessed via the "method" property of the "request" parameter.
    // The method "GET" means that we will be retrieving or reading information
    if(request.url == "/items" && request.method == 'GET'){
        response.writeHead(200, {'Content-Type': 'text/plain'});
        // Ends the response
        response.end('Data retrieved from gthe databse');
    }
    // The method "POST" means that we will be adding or creating informatoin
    // In this example, we will be just sending a text response for now

    if(request.url == '/items' && request.method == 'POST'){
        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end('Data to be sent to the database');

    }
}).listen(4000);
console.log('Server is running at localhost:4000')