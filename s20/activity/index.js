

// console.log("Hello World");

//Objective 1
// Create a function called printNumbers that will loop over a number provided as an argument.
	//In the function, add a console log to display the number provided.
	//In the function, create a loop that will use the number provided by the user and count down to 0
	//In the loop, create an if-else statement:

	// If the counter number value provided is less than or equal to 50, terminate the loop and exactly show the following message in the console:
	//"The current value is at " + count + ". Terminating the loop."

	// If the counter number value is divisible by 10, skip printing the number and show the following message in the console:
	//"The number is divisible by 10. Skipping the number."

	// If the counter number  value is divisible by 5, print/console log the counter number.


// ---------------------------

let count = prompt("Enter a number: ");
console.log("The number you provided is " + count + ".");
	function printNumbers() {

		while(count > 0) {
			
			count--;

			if (count <= 50) {
			console.log("The current value is at " + count + ". Terminating the loop.");
			break;
		} 
			else if(count %10===0 ) {
			console.log("The number is divisible by 10. Skipping the number.");
			continue;
	}

			else if (count %5===0){
				console.log(count);			
		} 				
	}
}

  printNumbers(count);

 // ---------------------




// ---------------

/*
8. Create a variable that will contain the string supercalifragilisticexpialidocious.
9. Create another variable that will store the consonants from the string.
10. Create another for Loop that will iterate through the individual letters of the string based on it’s length.
11. Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.
12. Create an else statement that will add the letter to the second variable.

*/

//Objective 2

var string = 'supercalifragilisticexpialidocious';
console.log(string);

var filteredString = string .split('');

//Add code here

var vowelFinder = /[aeiou]/ig;
var removeVowels = string .match(vowelFinder);

var noVowels = [];

	for (var i=0; i<filteredString.length; i++) 
	{
		if(removeVowels.indexOf(filteredString [i]) ===-1 ) {
			noVowels.push(filteredString [i])
		}
	}
	console.log(noVowels.join(''));





// The other method
/*var removeVowels = function(S) {

var filteredString = '';

for (let i = 0; i<S.length; i++) {

	if(S[i] === 'a' || S[i] === 'e' || S[i] === 'i' || S[i] === 'o' || S[i] === 'u') {
		 continue;
		}
			filteredString+=S[i];
	}
		return filteredString;
}


*/




























//Do not modify
//For exporting to test.js
try {
    module.exports = {
       printNumbers, filteredString
    }
} catch(err) {

}
