const http = require('http');

// creates a variable "port" to store the port number
const port = 4000;

// creates a variable "server" that stores the output of the "create server" method.
const server = http.createServer((request, response) => {
        if(request.url == '/greeting'){
        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end('Hello Again!')
    } else if(request.url == '/homepage'){
        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end('This is the homepage!')
    } else{
        response.writeHead(404, {'Content-Type': 'text/plain'});
        response.end('404 Page not available')
    }
    
});

// Uses the "server" and "port" variables created above
server.listen(port);

console.log(`Server now accessible at localhost:${port}`);