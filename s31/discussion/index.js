// "require" direcive- used to load node.js modules.
// "http module" -  lets the node.js transfer data using the HTTP
// "HTTP" - Protocol that allowws the fetching of resources
// We are now able to run a simple node.js server. Wherein when we added our URL in the browser, a client actually requested to our server was able to respond to a text.
// We used require() method to load node.js modules.
// a module is software component or part of a program which contains one or more routines
// The http module is a default module from node.js
// The http module let node.js tranfer data or let our client and server exchange data via hypertext transfer protocol.

let http = require("http");
/* 
- http.createServer() method allows us to create server and handle the request of a client
- request -  message sent by the client, usually via a web server
- response -  message sent by the server as an answer
*/

http.createServer(function(request, response){
    // res.writeHead is a method of the response object. this will allows us to add headers, which are additional information about our server's response. 
    // 'Context-Type' is one of the more recognizable headers, it is pertaining to the data type of the content we are responding with.
    // The first argument in writehead is an HTTP which is used to tell the client about the status of their request. 200 meaning OK.
    // HTTP means the resource your trying to access cannot be found. 403 means the resource you're trying to acces is forbidded or requires authentication.
    response.writeHead(200, {"Context-Type": "text/plain"});

    // res.end() is  METHOD of the response object which ends the server's response and send a message/data as a string.
    response.end("Hello World");

    // .listen() allows us to assign a port to a  server.
    // port is a virtual point where connections start and end.
    // http://localhost:4000 - localhost is your current and 4000 is the port number assigned to where the process.
    // server is listening or running from. port 4000 -  popularly used for backend applications
}).listen(4000);

// When server is running, console will print out the messgae
console.log('Server is running at localhost:4000');

// "ctrl + C" to terminate connection in git bash/terminal