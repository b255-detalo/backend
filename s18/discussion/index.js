console.log("Hello");


// Functions
 // Parameters and arguments
// Functions in JS are lines/blocks of codes that tell our device/application to perform certain tasks when they are called


/*
function printInput(){
	let nickname = prompt("Enter your nickname");

	console.log("Hi," + nickname);
}
printInput();

*/

//  For other cases, functionsa can also process data directly passed into it instead of relting on global variables and prompt.

function printName(name){
	console.log("My name is " + name);
};

printName("Juana");
printName("Juan");

// You can directly passed data into the function. The function can then call/use that data w/c is referred as "name".

//  Name isa a parameter acts as a named variable/container that exist only inside a function

// it is used to store informnation that is provided to a funtion when it is called/invoked.

// VARIABLES can also be passed as an argument
let sampleVariable = "Yui";
printName(sampleVariable);

// Function arguments cannot be used by a function if there are no parqameters provided w/in the function

function checkDivisilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + "divided by 8 is: " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + "divisible by 8?");
	console.log(isDivisibleBy8);

}
checkDivisilityBy8(64);
checkDivisilityBy8(28);

// Functions as arguments

// Function parameters can also accept other function as argumnets
// Some complex functions use other functions as results to perform more complicated results

function argumentFunction(){
	console.log("This function was passed as an argument before the message was printed");
}

function invokeFunction(argumentFunction){
	argumentFunction();
}

// Adding and removing "()" impacts the output of JS heavily
// when a funtion is used w/ parenthesis "()", it denotes invoking/calling a fiunction 
// A function used w/out a parenthesis is normally associated w/ using the function as an argument to another function

invokeFunction(argumentFunction);


// finding information about a function in the console.log
//  
console.log(argumentFunction);


// using multiple parameters 
// Multiple arguments will correspond to the number of parameters declared in a function in succeesing order.log
/*
function createFullName(firstName, middleName, lastName){

	console.log(firstName + ' ' iddleName + ' ' + lastName);
}

createFullName('Juan', 'Dela', 'Cruz');
// "Juan" will be stored in the parameter "firstName"
// "Dela" will be stored in the parameter "middleName"
// "Cruz" will be stored in the parameter "lastName"




createFullName('Juan', 'Dela');
createFullName('Juan', 'Dela', 'Cruz', 'Hello');
 
 */


// Using variables as arguments
let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

// createFullName(firstName, middleName, lastName);


// Parameter names are just names to refer to the argument. even if we change the name of the parameters, the arg. will be received in the  same order it was passed.


// The return statement
// The "return" statement allows us to output a value from a funtion to be passed to the line/block of code that invoked/called the function.

function returnFullName (firstName, middleName, lastName){
		return firstName + ' ' + middleName + ' '+ lastName
	console.log("This message will not be printed");	
}

// In our example, the "returnFullName" function was invoke was invoked/called in the same line as declaring a variable
// Whatever value is returned from the "returnFullName" function is stored in the "completeName" variable.

let completeName = returnFullName("Jeffrey", "Amazon", "Bezos");
console.log(completeName);


// This way , a function is able to return a value we can further use/manipulate in our program instead of only printing/dispolay it in the console.


// Mini Activity
// Create a function that will add two numbers together

// 1. you must use parameters and argument


/*

        function add(){
            const num1 = 5;
            const num2 = 3;

        // add two numbers
        const sum = num1 + num2;

            // display the sum
            console.log('The sum of ' + num1 + ' and ' + num2 + ' is: ' + sum);
        }
        // call function
        add();

        */








// ---------------------------

// Notice that the console.lof after the return is no longer printed in the console. that is because ideally any line/block of code that comes after the return statement is ignored because it ends the functi.on execution
// In this example, console.log() will print the return value of the returnFullName function.
console.log(returnFullName(firstName, middleName, lastName));

// You cann also create avariable inside the function to contain the result and return that variable instead.
function returnAddress(city, country){
	let fullAddress = city + ',' + country;
	return fullAddress; 
} 
let myAddress = returnAddress("Cebu City", "Cebu");
				console.log(myAddress);

	// on the other hand, when a function only has console.log() to display its results it will return undefined instead.

	function printPlayerInfo(username, level, job){
		console.log("Username: " + username);
		console.log("Level: " + level);
		console.log("Job: " + job);
	}

	let user1 = printPlayerInfo("Knight_white", 95, "Paladin");
	console.log(user1);