const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course");
const auth = require("../auth");

// Route for creating a course
// S39 ------------------ activity
router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true){
		console.log(req.body)
		courseController.addCourse(req.body).then(resultFromController =>
			res.send(resultFromController))
	} else{
		res.send(false);
	}
});
// ------------------------------
// Route for retrieving all the courses
router.get("/all", (req, res) => {
    courseController.getAllCourses().then(resultFromController => res.send (resultFromController))
});


// Route for retrieving all the ACTIVE courses
// Middleware for verifying JWT is not required because users who are not logged in should also be able to view the courses.
router.get("/", (req, res) => {
    courseController.getAllActive().then(resultFromController => res.send (resultFromController))
});


// Route for retrieving a specis=fic course
// Creasting a route using the "/:parameterName" creates a dynamic route, meaning the url is not static changes depending upon the information provided
router.get("/:courseId", (req, res) => {
    console.log(req.params.courseId);

    // since the course ID will be sent via the URL, we cannot retrieve it from tehe request body
    //  We can however retrieve the course ID by accessing the request's "params" property which contains all parameters provided via the url.
courseController.getAllCourses(req.params).then(resultFromController => res.send             (resultFromController));
})
 
// Route for updating a course
// JWT verification is needed for this route to ensure that the user is logged in before updating a course
router.put("/:courseId", auth.verify,(req, res) => {
    courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})


// ------------------- s40 -- Activity
// Route to archiving a course
// A "PUT" request is used instead of "DELETE" request because of our approach in archiving and hiding the courses from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases
router.put("/:courseId/archive", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController));
});



















// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;

 