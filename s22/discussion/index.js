

// Array Methods
// JAVASCRIPT has built-in functions for arrays. 
// This allows us to manipulate and access array items.
/*
	Mutators Methods
	- Mutator Methods are functions that "mutate" or change an array after they're created
	- These methods manipulate the original array performing various tasks such as ading and removing elements.

*/
let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];
// push()
/*
-Adds an elemnt in the end of an array AND returns the array's lenght
*/
console.log('Current array: ');
console.log(fruits);

let fruitsLength = fruits.push('Mango');//also returns length
console.log(fruitsLength);
console.log("Mutated array from push method: ");
console.log(fruits);

// Adding multiple elemets to an array
fruits.push('Avocado', 'Guava');
console.log("Mutated array from push method: ");
console.log(fruits);


// pop()
/*
Removes the last element in an aray AND returns the removed element/ mapakita ulit
*/
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from pop() method: ");
console.log(fruits);

/*fruits.pop();
console.log(fruits);*/ // removes the last item


// unshift()
/*Add an element at the beginning of an array*/ // sa unahan
fruits.unshift('Lime', 'Banana');
console.log("Mutated array from unshift method: ");
console.log(fruits);


// shift()
/*-Removes an element at the beginning of an array AND returns the removed element*/
let anotherFruit = fruits.shift();
console.log("Mutated array from shift() method: ");
console.log(fruits);


// splice()
/*Simultaneously removes elements from specified index number and adds elements/ *delete insert*
*/
fruits.splice(1, 2, 'Lime', 'Cherry');//pinalitan
console.log('Mutated array from splice() method');
console.log(fruits);



// sort()
/*Rearranges the array elemets in alphanumeric order*/
fruits.sort();
console.log('Mutated array from sort() method');
console.log(fruits);

/*
Important Note:
-The "Sort" method is used for more complicated sorting functions
*/


// reverse()
/*
	-Reverses the order of array elemets, PABALIKTAD
*/
fruits.reverse();
console.log('Mutated array from reverse method');
console.log(fruits);




// Non-mutator methods
/*Non-mutator methods are functions that do not modify or change an array after they're created.
- These methods do not manipulate orginal array performing various tasks such as returning elemets from an array and combining arrays  */

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];
// indexof()
/*
-Returns the index number of the first matching element found in an array
-If no match was found, the result will be -1 
-The search process will be done from first element proceeding to the last elemet
*/
let firstIndex = countries.indexOf('PH');
console.log('Result of indexOf method:' + firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log('Result of indexOf method:' + invalidCountry);


// lastIndexOf()
/*
	- Returns the index number of the last matching element found in an array
	- The search process will be done from the last element proceeding to the first element
*/

let lastIndex = countries.lastIndexOf('PH');// mag -1 pag small letters
console.log('Result of lastIndexOf method:' + lastIndex);

let lastIndexStart = countries.lastIndexOf('PH', 6);// 6, or any number specification on what index to start and find 'PH', will search from right-left <--
console.log('Result of lastIndexOf method:' + lastIndexStart);


// slice()
/*
	-Portions for slice element for an array AND returns a new array
	tapos lahat ttanggalin nya left the unspecified, only just return not totally removed
*/

console.log(countries);//display countries 

let sliceArrayA = countries.slice(2);// inalis yung 2 arrs US & PH
console.log('Result from slice method sliceArrayA');
console.log(sliceArrayA);


let sliceArrayB = countries.slice(2, 4);
console.log('Result from slice method sliceArrayB');
console.log(sliceArrayB);

let sliceArrayC = countries.slice(-5);
console.log('Result from slice method sliceArrayC');
console.log(sliceArrayC);



// toString()
/*
returns an array as a string separated by commas*/

let stringArray = countries.toString();
console.log('Result from toString method');
console.log(stringArray);



// concat()
/*Combines two arrays and returns the combined result*/
let taskArrayA = ['drink html', 'eat javaScript'];
let taskArrayB = ['inhale css', 'breathe sass'];
let taskArrayC = ['get git', 'be node'];

let tasks = taskArrayA.concat(taskArrayB);
console.log('Result from concat method');
console.log(tasks);


// Combining multiple arrays
console.log('result from concat method: ');
let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks);


// Combining arrays with elements
let combinedTasks = taskArrayA.concat('smell express', 'throw react');
console.log('result from concat method: ');
console.log(combinedTasks);


// join()
/*Returns an array as a string separated by specified separator string  */
let users = ['John', 'Jane', 'Joe', 'Robert'];
console.log(users.join());// will be printed w/ commas John,Jane,Joe,Robert
console.log(users.join('')); //empty dugtong dugtong lang JohnJaneJoeRobert
console.log(users.join(' - '));// with dash and space 'John - Jane - Joe - Robert'


// Iteration methods
/*Iteration methods are loops designed to perform repetitive tasks on arrays
-Iteration methods loops over all items in an array
- Useful for manipulating array data resulting in complex tasks
-Array iteration normally work with a function supplied as an argument
-How these function works is by performing tasks that are pre-defined within an array's method
*/


// for Each()
/*
Similar to a for loop that iterates on each array element
- for each item in the aray, the anonymous function passed in the forEach() method will be run.
_The anonymous function is able to receive the current item being iterated or loop over by assigning a parameter
-it's common practice the singular form of the array content for parameter names used in array loops.
*/

allTasks.forEach(function(task){
	console.log(task)
});

// Using forEach with conditional statements
let filteredTasks = [];//need to deacler empty array

	allTasks.forEach(function(task){
		if(task.length > 10){
			filteredTasks.push(task);
	}
})

console.log("result of filtered tasks");
console.log(filteredTasks);


// map()

/*Iterates on each element AND returns new array with different values depnding of the result  of the  functions operations
- This is useful for performing tasks where mutating/changing the elements are required
- Unlike the forEach method, the map method requires the use of a 'return' statememnt in order to create another array with the perform operation */

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(numbers) {//anonymous function
	return numbers * numbers; //multiply by itself will be [1, 4, 9, 16, 25], or + - / etc
})

console.log("original array: ");
console.log(numbers);// original array unaffected by map()
console.log("result of a map() tasks, multiplied by itself");
console.log(numberMap);//Anew array is returned by map()


// map() vs forEach()
let numberForEach = numbers.forEach(function(numbers) {
	return numbers * numbers;
});
console.log(numberForEach);

// forEach loops over all items in the array as does map() but forEach() does not return a new array.



// every()
/*Checks all elements in an array met the given condition
- this is useful  for validating with large amounts of data 
-returns a true value if all elements meet the condition and false if all elements meet the condition and false if otherwise */

let allValid = numbers.every(function(numbers) {
	return(numbers < 3);
})
console.log("Result of every() method: ");
console.log(allValid);


// some()
/*
-Checks if at least 1 element in the array meets thge gioven condition
-returns a true value if at least one elemet meets the condition and false if otherwise.
*/

let someValid = numbers.some(function(numbers){

	return (numbers < 2)
});

console.log("Result of some method: ");
console.log(someValid);


// filter()
/*
 -Returns new array that contains elements w/c mets the given condition
 -returns an empty array if no elemets were found 
 -useful for filtering array with a given condition and shortens the syntax compared to using to other array iteration methods
 - mastery of loops can helpo us work effectiufvely by reducing amount of code we use.

*/
let filterValid = numbers.filter(function(numbers){
	return (numbers < 3 );
});

console.log("Result of filter() method: ");
console.log(filterValid);



// no elements found
let nothingFound = numbers.filter(function(numbers){
	return (numbers < 0 );
});

console.log("Result of filter() method: ");
console.log(nothingFound);


// filtering using forEach()
let filteredNumbers = [];

numbers.forEach(function(numbers){

	if(numbers < 3){
		filteredNumbers.push(numbers);
	}
});

console.log("Result of forEach() method: ");
console.log(filteredNumbers);


// includes()
/*Includes() method checks if the argument passed can be found in the array 
-it returns a boolean whiich can be saved in a variable 
-returns true if the argument is found in the array
-returns false if is not
*/

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monoitor'];

let productFound1 = products.includes('Mouse');
console.log(productFound1);

let productFound2 = products.includes('Headset');
console.log(productFound2);

/*
-Method can be chained using them one after another 
-The result of the first method is uased on the second method untill all 'chained' methods have been resolved
*/

let filteredProducts = products.filter(function(products){

	return products.toLowerCase().includes('a');// will check all the array with 'a'
});
console.log(filteredProducts);


// reduce()
/*
 - Evaluates elements from left to right and returns/ reduces the array into a single value

*/
let iteration = 0;
console.log(numbers);

let reducedArray = numbers.reduce(function(x ,y){
		//used to track the current iteraion count and accumulator/currentValue data
	console.log('Currnt iteration' + ++iteration);
	console.log('accumulator: ' + x);
	console.log('currentValue: ' + y);

	// the operation to reduce the+ array into a single value
	return x + y;

})

console.log("Result of reduce method: "  + reducedArray);


// reducing string arrays
let list = ['Hello', 'Again', 'World'];

let reducedJoin = list.reduce ( function(x, y) {
	return x + ' ' + y;} );

console.log("result of reduce method: " + reducedJoin);



//------------------------------------------------
