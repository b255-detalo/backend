




/*
    Create functions which can manipulate our arrays.
*/

// console.log(registeredUsers);
let friendList = [];
let registeredUsers = [

    "James Jeffries", // 0 
    "Gunther Smith",//1
    "Macie West",//2
    "Michelle Queen",//3
    "Shane Miguelito",//4
    "Fernando Dela Cruz",//5
    "Akiko Yukihime" //6
    ];
/*
   1. Create a function called register which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, return the message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and return the message:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.
*/
let inputFromUser = '';

function register(inputFromUser){
        
         if (registeredUsers.indexOf(inputFromUser) >= 0) {
            return("Registration failed. Username already exists!");      
         } 
         else  {
            registeredUsers.push(inputFromUser);
            return('Thank you for registering!');              
         }         
    }
            register(inputFromUser);



/*
    2. Create a function called addFriend which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then return the message with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, return the message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.
*/

function addFriend(inputFromUser){
        
         if (registeredUsers.indexOf(inputFromUser) >= 0) {
            friendList.push(inputFromUser)//storing new strings/list
            return("You have added " + inputFromUser + " as a friend!");      
         } 
         else  {
            
            return("User not found.");              
        }         
    }

        addFriend();





/*
3. Create a function called displayFriends which will allow us to show/display the items in the 
   friendList one by one on our console.
        - If the friendsList is empty return the message: 
        - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/  


function displayFriends() {

    if (friendList && Object.entries(friendList).length === 0) { //Checks if string is empty
        return("You currently have 0 friends. Add one first.");
    } 

    else {
        return(friendList); //friendList's updated version will be displayed.
    }  

}
        displayFriends();



/*
    4. Create a function called displayNumberOfFriends which will display the amount of registered users in your friendsList.
        - If the friendsList is empty return the message:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty return the message:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/
//----count number the of strings
/*function wordCount(str){

    return str.split(' ').length;
}
console.log(wordCount('word1 word2'));*/

// var count = (friendList.match(/is/g) || []).length;


function displayNumberOfFriends() {

    if (friendList && Object.entries(friendList).length === 0) { //Checks if string is empty
        return("You have 0 friends. Add one first.");
    } 

    else {
        return("You currently have 1 friends."); //number of friends will shown
    }  
}
        displayNumberOfFriends();


//-----------

/*
    5. Create a function called deleteFriend which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty return a message:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/


function deleteFriend() {

    if (friendList && Object.entries(friendList).length !== 0) { //Checks if string is empty
        friendList.pop();
        
    } 

    else {
        return("You currently have 0 friends. Add one first.");
    }  
}
        deleteFriend();

















/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/














//For exporting to test.js
try {
    module.exports = {
        registeredUsers,
        friendsList,
        register,
        addFriend,
        displayFriends,
        displayNumberOfFriends,
        deleteFriend
    }
}
catch(err) {

}


