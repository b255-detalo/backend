// Set up dependencies
const express = require("express");
const mongoose =  require("mongoose");

// This allows us to use all of the routes defined in "taskRoute.js"
const taskRoute = require("./routes/taskRoute");

// Server
const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Database connection
// Connecting to mongoDB Atlas
mongoose.connect("mongodb+srv://apollo-255:admin123@zuitt-bootcamp.dckhweo.mongodb.net/?retryWrites=true&w=majority", 
    {
        useNewUrlParser : true, 
        useUnifiedTopology: true
    }

);

// Add the task route
// Allows all the task routes created in the "taskRoute.js" file to use the "/tasks" route.
app.use("/tasks", taskRoute);
// example http://localhost:4000/tasks/getTasks

// Server Listening
if(require.main === module) {
    app.listen(port, () => console.log(`Server is running at ${port}`));
}
module.exports = app;

