// Contains all the endpoints of our application
// We separate the routes such that app.js only contains information on the server
// We need to use the express Router() function to achieve this
const express = require("express");
// Creates a Router instance that functions as a middlewawre and routing system.
// Allows access to HTTP middleware that makes it easier to reate routes for our application.
const router = express.Router();

// The "taskControler" allows us to use the functions defined in th "taskController.js" file
const taskController = require("../controllers/taskController");

// [SECTION] Routes
// The routes are responsible for defining the URIs that our client access and the corresponding controller functions that will be used when the route is accessed
// They invoked the controller functions from the controller files

// Routes to get all the tasks
// This route exprects to receive a GET request at the URL "/tasks"
router.get("/", (req, res) => {
    
    // Invokes the "getAllTasks" function from the "taskController.js" file and sends the result to postman

    // The "resultFromController" is only used here to make the code easier to understand but it's common  practice to use the shorthand parameter name for a result using the parameter name "result/res"
    taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})
 
router.post("/", (req, res) => {
    taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
     
})

// Route to delete a task
// The task id is obtained from the URL is denoted by the ":id" indentifier in the route
// The colon(:) is an identifier that helps create a dynamic route which allowsd us to supply information in the URL
router.delete("/:id" , (req, res) => {
        taskController.deleteTask(req.params.id).then(resultFromController => res.send
        (resultFromController));
})




router.put("/:id", (req, res) => {
    taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send
        (resultFromController));
})


// ------------------ 
router.get("/:id", (req, res) => {
    taskController.getTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
     
});


//-------------------
 router.get("/:id/complete", (req, res) => {
    taskController.getTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
     
});
 


















// Use "module.exports" to export the router object to use in the "app.js"
module.exports = router;